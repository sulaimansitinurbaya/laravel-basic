<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Daftar Fitur</title>
</head>
<body>
  <h4>Daftar Fitur</h4>
  <ul>
    <li>
      <a href="{{url('user')}}">Daftar Pengguna</a>
    </li>
    <li>
      <a href="{{url('category')}}">Kategori</a>
    </li>
    <li>
      <a href="{{url('artikel')}}">Artikel</a>
    </li>
    <li>
      <a href="{{url('profile')}}">Profile</a>
    </li>
    <li>
      <a href="{{url('logout')}}">Logout</a>
    </li>
  </ul>
</body>
</html>