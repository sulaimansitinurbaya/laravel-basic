<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Edit Kategori</title>

  <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?ls=25070857')}}" />
</head>
<body>

  <form action="{{url('category/update')}}" method="post">
    @csrf
    <input type="hidden" name="id" value="{{$item->id}}" />
    <x-partial.input
      label="Judul Kategori" placeholder="Masukan judul kategori"
      id="titleInput" name="title" value="{{$item->title}}" />
    <a href="{{url('/category')}}">Kembali</a>
    <button type="submit">Simpan</button>
  </form>

</body>
</html>