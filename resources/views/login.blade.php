<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?used=08082023-0824')}}" />
</head>
<body>
    <div class="card box-shadow radius-10px">
    <form action="{{url('authentication')}}" method="post">
        @csrf
        <div class="mb-10px">
            <label for="emailInput">Email</label>
            <input type="email" name="email" placeholder="Masukkan Email" required />
        </div>

        <div class="mb-10px">
            <label for="passwordInput">Password</label>
            <input type="password" name="password" placeholder="Masukkan Password" required />
        </div>

        <button type="submit">Masukkan</button>

    </form>
</div>
</body>
</html>