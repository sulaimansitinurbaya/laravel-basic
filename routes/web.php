<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->group(function () {
     Route::post('/authentication','index');

     Route::get('/', 'loginPage')->name('login');

});

/**
 * middleware(['auth:sanctum' di gunakan utk mencegah user yg blm login masuk 
 * hanya user yg udh login yg di beri akses masuk
 */
Route::middleware(['auth:sanctum', 'verified'])->group(function () {

Route::get('/dashboard', function () {
    return view('index');
});

Route::get('logout', [AuthController::class, 'logout']);
Route::get('profile', [AuthController::class, 'profile']);

   

});


Route::prefix('user')->group(function () {
    Route::get('', [UsersController::class, 'index']);
    Route::get('edit/{id}', [UsersController::class, 'edit']);
    Route::post('store', [UsersController::class, 'store']);
    Route::post('update', [UsersController::class, 'update']);
    Route::post('delete', [UsersController::class, 'delete']);
});

Route::prefix('category')->controller(CategoryController::class)->group(function () {
    Route::get('/', 'index');
    Route::get('/edit/{id}', 'edit');
    Route::post('/store', 'store');
    Route::post('/update', 'update');
    Route::post('/delete', 'delete');
});

Route::prefix('pages')
->controller(PageController::class)->group(function () {
    Route::get('/', 'index');
    Route::get('/edit/{id}', 'edit');
    Route::post('/store', 'store');
    Route::post('/update', 'update');
    Route::post('/delete', 'delete');
});

