<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use InvalidArgumentException;



class AuthController extends Controller
{
    function index(Request $req){
        try{
            /*untuk mencari data user berdasarkan email */
            $find = User::where('email', $req->email)->first();

        
            
            /**
             * untuk mengecek apakah user di temukan atau tidak
             * jika tidak di temukan maka akan muncul notifikasi di bawah
             */
            if(!$find){
                throw new InvalidArgumentException('Akun tidak di temukan', 500);
            }

            /**
             * untuk mengecek kecocokan password
             * yang ada pada database
             */
            if (!Hash::check($req->password, $find->password)) {
                throw new InvalidArgumentException('Email atau password salah!', 500);
            }

            /**
             * auth : untuk memberi akses login
             * sebagai "web"
             */
            Auth::guard('web')->login($find, $remember = true);
            
            /**
             * untuk otomatis berpindah ke halaman 'dashboard'
             */
            return redirect('dashboard');
           
        } catch (\Throwable $th){
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
    function logout(Request $request){
        /**
         * untuk menghapus sesi login / masuk sebagai 'web'
         */
        Auth::guard('web')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }
    

    /**
     * untuk memvalidasi
     * dimana jika sudah login
     * akan di arahkan ke halaman dashboard
     */
    function loginPage(){
        if (auth()->user()) {
            return redirect('dashboard');
        }
        return view('login');
    }

    function profile(){
        $item = User::find(auth()->user()->id);

        if (!$item) {
            return abort(404);
        }

        return view('user.edit', [
            'item' => $item,
            'profile' => true
        ]);
    }
}
